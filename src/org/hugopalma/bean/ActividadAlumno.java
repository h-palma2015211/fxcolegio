/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.bean;

/**
 *
 * @author HUAL
 */
public class ActividadAlumno {
    
    private Alumno alumno;
    private Actividad actividad;

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public Alumno getAlumno() {
        return alumno;
    }
    
    
    
}
