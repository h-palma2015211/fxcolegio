/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.bean;

/**
 *
 * @author HUAL
 */
public class Usuario {
    private int idUsuario;
    private String nombre;
    private String clave;
    
    public Usuario() {
    }
    
    /**
     *
     */
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    public int getIdUsuario() {
        return idUsuario;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
    
    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    
}
