/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.bean;

/**
 *
 * @author HUAL
 */
public class MateriaSeccionProfesor {
    private int idMateriaSeccionProfesor;
    private Seccion seccion;
    private Profesor profesor;

    public void setIdMateriaSeccionProfesor(int idMateriaSeccionProfesor) {
        this.idMateriaSeccionProfesor = idMateriaSeccionProfesor;
    }


    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    public int getIdMateriaSeccionProfesor() {
        return idMateriaSeccionProfesor;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public Seccion getSeccion() {
        return seccion;
    }
    
    
}
