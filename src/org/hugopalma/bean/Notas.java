/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.bean;

/**
 *
 * @author HUAL
 */
public class Notas {
    private int idNotas;
    private short bimestre;
    private int nota;
    private int idAlumno;
    private int  idMateria;
    private int idActividad;

    public void setBimestre(short bimestre) {
        this.bimestre = bimestre;
    }

    public void setIdAlumno(int idAlumno) {
        this.idAlumno = idAlumno;
    }

    public void setIdMateria(int idMateria) {
        this.idMateria = idMateria;
    }

    public void setIdActividad(int idActividad) {
        this.idActividad = idActividad;
    }
    

    public void setIdNotas(int idNotas) {
        this.idNotas = idNotas;
    }

    

    public void setNota(int nota) {
        this.nota = nota;
    }

    public short getBimestre() {
        return bimestre;
    }

    public int getIdAlumno() {
        return idAlumno;
    }

    public int getIdMateria() {
        return idMateria;
    }

    

    public int getIdNotas() {
        return idNotas;
    }

    public int getIdActividad() {
        return idActividad;
    }

    

    public int getNota() {
        return nota;
    }
    
    
}
