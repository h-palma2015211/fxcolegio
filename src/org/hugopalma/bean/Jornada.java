/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.bean;

/**
 *
 * @author HUAL
 */
public class Jornada {
    private int idJornada;
    private String nombre;

    public void setIdJornada(int idJornada) {
        this.idJornada = idJornada;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdJornada() {
        return idJornada;
    }

    public String getNombre() {
        return nombre;
    }
    @Override
    public String toString() {
        return this.nombre;
    }
        
    
}
