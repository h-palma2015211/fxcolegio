/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.bean;

/**
 *
 * @author HUAL
 */
public class Seccion {
    private int idSeccion;
    private String nombre;
    private Grado grado;
    private Jornada jornada;

    public void setIdSeccion(int idSeccion) {
        this.idSeccion = idSeccion;
    }

    public void setJornada(Jornada jornada) {
        this.jornada = jornada;
    }

    public Jornada getJornada() {
        return jornada;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdSeccion() {
        return idSeccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setGrado(Grado grado) {
        this.grado = grado;
    }

    public Grado getGrado() {
        return grado;
    }
    
    @Override
    public String toString() {
        return this.nombre;
    }
    
    
    
}
