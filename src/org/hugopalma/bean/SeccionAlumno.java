/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.bean;

/**
 *
 * @author HUAL
 */
public class SeccionAlumno {
    private int idSeccionAlumno;
    private Seccion idSeccion;
    private Alumno idAlumno;

    public void setIdAlumno(Alumno idAlumno) {
        this.idAlumno = idAlumno;
    }

    public void setIdSeccion(Seccion idSeccion) {
        this.idSeccion = idSeccion;
    }

    public void setIdSeccionAlumno(int idSeccionAlumno) {
        this.idSeccionAlumno = idSeccionAlumno;
    }

    public Alumno getIdAlumno() {
        return idAlumno;
    }

    public Seccion getIdSeccion() {
        return idSeccion;
    }

    public int getIdSeccionAlumno() {
        return idSeccionAlumno;
    }

    
    
    
    
    
}
