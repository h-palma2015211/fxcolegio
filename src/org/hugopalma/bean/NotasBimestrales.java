/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.bean;

/**
 *
 * @author HUAL
 */
public class NotasBimestrales {
    private int idNotasBimestrales;
    private short bimestre;
    private Materia materia;
    private Alumno alumno;
    private short nota;

    public void setNota(short nota) {
        this.nota = nota;
    }

    public short getNota() {
        return nota;
    }
    
    

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public void setBimestre(short bimestre) {
        this.bimestre = bimestre;
    }

    public void setIdNotasBimestrales(int idNotasBimestrales) {
        this.idNotasBimestrales = idNotasBimestrales;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public short getBimestre() {
        return bimestre;
    }

    public int getIdNotasBimestrales() {
        return idNotasBimestrales;
    }

    public Materia getMateria() {
        return materia;
    }
    
    
    
    
    
}
