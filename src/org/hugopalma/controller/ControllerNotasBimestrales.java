/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.NotasBimestrales;
import org.hugopalma.conecction.SQLDatabaseConnection;
import org.hugopalma.ui.CRUDSeccion;
import org.hugopalma.ui.Nota;
/**
 *
 * @author HUAL
 */
public class ControllerNotasBimestrales {
    private static final ControllerNotasBimestrales CONTROLLER_NOTAS_BIMESTRALES = new ControllerNotasBimestrales();
    
    private ArrayList<NotasBimestrales> arrayList;
    
    private ControllerNotasBimestrales() {
        this.arrayList = new ArrayList<>();
        getArrayList();
                
    }
    public static ControllerNotasBimestrales getCONTROLLER_NOTAS_BIMESTRALES() {
        return CONTROLLER_NOTAS_BIMESTRALES;
    } 
    
    
    public ArrayList<NotasBimestrales> getArrayList() {
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM NotasBimestrales;");
        
        try {
            while (resultSet.next()) {
                NotasBimestrales notasBimestrales = new NotasBimestrales();
                notasBimestrales.setBimestre(resultSet.getShort("bimestre"));
                notasBimestrales.setNota(resultSet.getShort("nota"));
                notasBimestrales.setMateria(ControllerMateria.getCONTROLLER_MATERIA().search(resultSet.getInt("idMateria")));
                notasBimestrales.setAlumno(ControllerAlumno.getCONTROLLER_ALUMNO().search(resultSet.getInt("idAlumno")));
                arrayList.add(notasBimestrales);
                
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerNotasBimestrales.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrayList;
        
    }
    
    public void add(short bimestre, int idMateria, int idAlumno) {
        
        
        if (verificacion(bimestre, idMateria, idAlumno) == false) {
            SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO NotasBimestrales(bimestre, idMateria, idAlumno, nota) VALUES(" +bimestre+ ", " +idMateria+ ", " +idAlumno+ ", (SELECT SUM(nota) FROM Notas WHERE idAlumno= "+idAlumno+" AND idMateria=" +idMateria+ " AND bimestre= " +bimestre+ "));");
            Nota.getNOTA().stageNota(idAlumno);
        }
      }
    
    public boolean verificacion(short bimestre, int idMateria, int idAlumno) {
        for (NotasBimestrales s : arrayList) {
            if (s.getBimestre() == bimestre && s.getAlumno().getIdAlumno() == idAlumno && s.getMateria().getIdMateria() == idMateria) {
                return true;
            }
        }
        return false;
    }
    
    public NotasBimestrales search(int idAlumno) {
        getArrayList();
        for (NotasBimestrales s : arrayList) {
            System.out.println(s.getAlumno().getIdAlumno());
            if (s.getAlumno().getIdAlumno() == idAlumno) {
                return s;
            }
        }
        return null;
    }
}
