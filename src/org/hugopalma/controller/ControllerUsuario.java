/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.GridPane;
import javax.swing.JOptionPane;
import org.hugopalma.bean.Usuario;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerUsuario {
    private static final ControllerUsuario CONTROLLER_USUARIO =
            new ControllerUsuario();
    private ArrayList<Usuario> arrayList;
   
    private ControllerUsuario() {
        this.arrayList = new ArrayList<>();
        
    } 
    
    
    public static ControllerUsuario getCONTROLLER_USUARIO() {
        return CONTROLLER_USUARIO;
    }
    
    
    
    public void add(String nombre, String clave) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO Usuario(nombre, clave) VALUES ('" + nombre + "', '" + clave + "')");
    }
    
    public ArrayList<Usuario> buscar(String criterio) {
        ArrayList<Usuario> listaResultado = new ArrayList<>();
        for (Usuario usuario : arrayList) {
            if (usuario.getNombre().equals(criterio) || String.valueOf(usuario.getIdUsuario()).equals(criterio)) {
                listaResultado.add(usuario);
                return listaResultado;
                
            }
        }
        return null;
    }
    
    public String verificador(String nombre, String clave) {
        arrayList.clear();
        getArrayList();
        for (Usuario explo : arrayList) {
            if (nombre.equals(explo.getNombre()) && clave.equals(explo.getClave()) ) {
                return nombre;
            }
        }
      
        return null;
    }
    public ArrayList<Usuario> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION()
                .query("SELECT * FROM Usuario");
       
        try {
            while (resultSet.next()) {
                Usuario usuario = new Usuario();
                usuario.setIdUsuario(resultSet.getInt("idUsuario"));
                usuario.setNombre(resultSet.getString("nombre"));
                usuario.setClave(resultSet.getString("clave"));
                arrayList.add(usuario);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().disconnect();
        return arrayList;
    }
    
    public void modify(String nombre, String clave, int idUsuario) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("UPDATE Usuario SET nombre = '" + nombre + "', clave = '" + clave + "'WHERE idUsuario = " + idUsuario +  ";" );
    }
    
    public void delete(int idUsuario) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM Usuario WHERE idUsuario = " + idUsuario + ";");
    }
}
 