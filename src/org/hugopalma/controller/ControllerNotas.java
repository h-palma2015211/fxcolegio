/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.Actividad;
import org.hugopalma.bean.Notas;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerNotas {
    private static final ControllerNotas CONTROLLER_NOTAS
            = new ControllerNotas();
    
    private ArrayList<Notas> arrayList;
    private ControllerNotas() {
        this.arrayList = new ArrayList<>();
    }
    
    public static ControllerNotas getCONTROLLER_NOTAS() {
        return CONTROLLER_NOTAS;
    }
    
    public void Create(Actividad actividad, int idAlumno, int nota, int idMateria) {
        if(verificacion(actividad.getIdActividad(), actividad.getBimestre(), idAlumno) == false) {
            SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery(
                    "INSERT INTO Notas(bimestre, nota, idAlumno, idMateria,"
                            + " idActividad) VALUES(" +actividad.getBimestre()+ ","
                    + " " +actividad.getValor()*nota/100 + ", " +idAlumno+ ", "
                            + "" +idMateria+ ", " +actividad.getIdActividad()+ ");");
        } else {
            
        
        }
    
    
    }
    public ArrayList<Notas> getArrayList() {
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM Notas");
        
        try {
            while (resultSet.next()) {
                Notas notas = new Notas();
                notas.setIdNotas(resultSet.getInt("idNotas"));
                notas.setBimestre(resultSet.getShort("bimestre"));
                notas.setIdAlumno(resultSet.getInt("idAlumno"));
                notas.setIdMateria(resultSet.getInt("idMateria"));
                notas.setIdActividad(resultSet.getInt("idActividad"));
                arrayList.add(notas);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerNotas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrayList;
    
    }
    
    
    

    public boolean verificacion(int idActividad, short bimestre, int idAlumno) {
        for (Notas notas : arrayList) {
            if (notas.getBimestre() == bimestre && notas.getIdAlumno() == idAlumno && notas.getIdActividad() == idActividad) {
                return true;
            }
        }
        return false;
    }
}
