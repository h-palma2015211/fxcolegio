/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.io.InputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerReporte {
    private static final ControllerReporte CONTROLLER_REPORTE = new ControllerReporte();
    
    
    private ControllerReporte() {}
    
    public static ControllerReporte getCONTROLLER_REPORTE() {
        return CONTROLLER_REPORTE;
    }
    //HACER EL GET CONNECTION PARA EL REPORTE.
    public void generate(Map parameters, String reportFile, String title) {
        try {
            InputStream reporte = getClass().getResourceAsStream("/org/hugopalma/resource/" + reportFile);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reporte);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().getConnection());
            JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
            jasperViewer.setTitle(title);
            jasperViewer.setVisible(true);
            
        } catch (JRException ex) {
            Logger.getLogger(ControllerReporte.class.getName()).log(Level.SEVERE, null, ex);
        }
             
    }
    
}
