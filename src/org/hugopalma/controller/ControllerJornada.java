/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.Jornada;
import org.hugopalma.conecction.SQLDatabaseConnection;



/**
 *
 * @author HUAL
 */
public class ControllerJornada {
    private static final ControllerJornada CONTROLLER_JORNADA = 
            new ControllerJornada();
    private ArrayList<Jornada> arrayList;
    
    private ControllerJornada() {
        this.arrayList = new ArrayList<>();
        getArrayList();
    }
    public static ControllerJornada getCONTROLLER_JORNADA() {
        return CONTROLLER_JORNADA;
    }
    public ArrayList<Jornada> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM Jornada");
        try {
            while (resultSet.next()) {
               Jornada jornada = new Jornada();
               jornada.setIdJornada(resultSet.getInt("idJornada"));
               jornada.setNombre(resultSet.getString("nombre"));
               arrayList.add(jornada);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerJornada.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().disconnect();
        return arrayList;
    }
    
    public void modify(String nombre, int idJornada) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("UPDATE Jornada SET nombre = '" + nombre + "'WHERE idUsuario = " + idJornada +  ";");
    }
    
    public void delete(int idJornada) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM Jornada WHERE idJornada = " + idJornada + ";");
    }
    
    public ArrayList<Jornada> buscar(String texto) {
        ArrayList<Jornada> lista = new ArrayList<>();
        
        for (Jornada jornada: arrayList) {
            if (jornada.getNombre().equals(texto) || String.valueOf(jornada.getIdJornada()).equals(texto) ) {
                lista.add(jornada);
                return lista;
            }
        }
        return null;
    }
    
    
    public void add(String nombre) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO Jornada(nombre) VALUES ('" + nombre + "')");
    }
    
    public Jornada search(int idJornada) {
        for (Jornada jornada : arrayList) {
            if (jornada.getIdJornada() == idJornada) {
                return jornada;
            }
        }
        return null;
    }
    
}
