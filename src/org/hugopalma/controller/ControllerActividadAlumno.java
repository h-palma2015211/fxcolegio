/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.ActividadAlumno;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerActividadAlumno {
    private static final ControllerActividadAlumno CONTROLLER_ACTIVIDAD_ALUMNO 
            = new ControllerActividadAlumno();
    
    private ArrayList<ActividadAlumno> arrayList;
    
    private ControllerActividadAlumno() {
        this.arrayList = new ArrayList<>();
    }
    public static ControllerActividadAlumno getCONTROLLER_ACTIVIDAD_ALUMNO() {
        return CONTROLLER_ACTIVIDAD_ALUMNO;
    }
    
    private ArrayList<ActividadAlumno> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM ActividadAlumno");
        
        
        try {
            while (resultSet.next()) {
                ActividadAlumno actividadAlumno = new ActividadAlumno();
                actividadAlumno.setActividad(ControllerActividad.getCONTROLLER_ACTIVIDAD().search(resultSet.getInt("idActividad")));
                actividadAlumno.setAlumno(ControllerAlumno.getCONTROLLER_ALUMNO().search(resultSet.getInt("idAlumno")));
                arrayList.add(actividadAlumno);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerActividadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return arrayList;
        
    }
    
    public void add(int idActividad, int idAlumno) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO ActividadAlumno(idActividad, idAlumno) VALUES( "+idActividad+ ", " +idAlumno+ ");");
    }
    
    public void delete(int idActividad) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM ActividadAlumno WHERE idActividad= " +idActividad+ ";");
    }
   
    
}
