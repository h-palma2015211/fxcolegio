/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.Administracion;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerAdministracion {

    private static final ControllerAdministracion CONTROLLER_ADMINISTRACION =
            new ControllerAdministracion();

    private ArrayList<Administracion> arrayList;

    private ControllerAdministracion(){
        this.arrayList = new ArrayList<>();
    }
    private int sueldo1;

    public static ControllerAdministracion getCONTROLLER_ADMINISTRACION() {
        return CONTROLLER_ADMINISTRACION;
    }

    public void add(String nombre, String puesto, int sueldo){
         SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("INSERT INTO Administracion(nombre, puesto, sueldo) VALUES ('" + nombre + "', '" + puesto + "', " + sueldo + ")");
    }

    public ArrayList<Administracion> buscar(String busqueda) {
        ArrayList<Administracion> lista = new ArrayList<>();

        for (Administracion administracion : arrayList) {
            if (administracion.getNombre().equals(busqueda) || String.valueOf(administracion.getId()).equals(busqueda)) {
                lista.add(administracion);
                return lista;
            }
        }
        return null;
    }

    public ArrayList<Administracion> getArrayList() {
        arrayList.clear();

        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION()
                .query("SELECT * FROM Administracion");

        try {
            while (resultSet.next() ) {
                Administracion administracion = new Administracion();
                administracion.setId(resultSet.getInt("id"));
                administracion.setNombre(resultSet.getString("nombre"));
                administracion.setPuesto(resultSet.getString("puesto"));
                administracion.setSueldo(resultSet.getInt("sueldo"));
                arrayList.add(administracion);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerAdministracion.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().disconnect();
        return arrayList;

    }
    
    public void modify(String nombre, String puesto, int sueldo, int id) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("UPDATE Administracion SET nombre = '" + nombre + "', puesto = '" + puesto + "', sueldo = " + sueldo + " WHERE id = " + id +  ";");
    
    }
    public void delete(int id) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM Administracion WHERE id = " + id + ";");
    }
    

}