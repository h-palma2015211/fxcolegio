/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.controller;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hugopalma.bean.Alumno;
import org.hugopalma.conecction.SQLDatabaseConnection;

/**
 *
 * @author HUAL
 */
public class ControllerAlumno {
    private static final ControllerAlumno CONTROLLER_ALUMNO  = new 
        ControllerAlumno();
    private ArrayList<Alumno> arrayList;
    private ArrayList<Alumno> arrayList2;
    private ArrayList<Alumno> arrayList3;
    private ControllerAlumno() {
        this.arrayList = new ArrayList<>();
        this.arrayList2 = new ArrayList<>();
        this.arrayList3 = new ArrayList<>();
        getArrayList();
      
    }
    
    public static ControllerAlumno getCONTROLLER_ALUMNO() {
        return CONTROLLER_ALUMNO;
    }
    
    public ArrayList<Alumno> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQL_DATABASE_CONNECTION()
                .query("Select * FROM Alumno");
        
        try {
            while (resultSet.next()) {
                Alumno alumno = new Alumno();
                alumno.setIdAlumno(resultSet.getInt("idAlumno"));
                alumno.setNombre(resultSet.getString("nombre"));
                arrayList.add(alumno);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().disconnect();
        return arrayList;
    }
    
    public void add(String nombre) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery(" INSERT INTO Alumno (nombre) VALUES('" + nombre + "')");
    
    }
    
    public void modify(String nombre, int idAlumno) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("UPDATE Alumno SET  nombre = '" +nombre+ "' WHERE idAlumno = " + idAlumno + ";");
    }
    
    public void delete(int idAlumno) {
        SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().executeQuery("DELETE FROM Alumno WHERE idAlumno = " +idAlumno+ ";");
    }
    
    public Alumno search(int idAlumno) {
        for (Alumno alumno : arrayList) {
            if (alumno.getIdAlumno() == idAlumno) {
                return alumno;
            }
        }
        return null;
    }
    
    
    
    public ArrayList<Alumno> buscar(String criterio) {
        ArrayList<Alumno> listaResultado = new ArrayList<>();
        for (Alumno alumno : arrayList) {
            if (alumno.getNombre().equals(criterio) || String.valueOf(alumno.getIdAlumno()).equals(criterio)) {
                listaResultado.add(alumno);
                return listaResultado;
                
            }
        }
        return null;
    }
    
    public ArrayList<Alumno> getArrayList2(int idSeccion) {
        arrayList2.clear();
        ResultSet result =   SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM Alumno WHERE Alumno.idAlumno IN(SELECT SeccionAlumno.idAlumno FROM SeccionAlumno WHERE SeccionAlumno.idSeccion = " + idSeccion + ") ORDER BY Alumno.nombre");
        
        try {
            while (result.next()) {
                Alumno alumno = new Alumno();
                alumno.setIdAlumno(result.getInt("idAlumno"));
                alumno.setNombre(result.getString("nombre"));
                arrayList2.add(alumno);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return arrayList2;
    }
    
    public ArrayList<Alumno> getArrayList3(int idSeccion) {
       arrayList3.clear();
      
        ResultSet result =   SQLDatabaseConnection.getSQL_DATABASE_CONNECTION().query("SELECT * FROM Alumno WHERE Alumno.idAlumno  NOT IN(SELECT SeccionAlumno.idAlumno FROM SeccionAlumno WHERE SeccionAlumno.idSeccion = " + idSeccion + ") ORDER BY Alumno.nombre");
        
        try {
            while (result.next()) {
                Alumno alumno = new Alumno();
                alumno.setIdAlumno(result.getInt("idAlumno"));
                alumno.setNombre(result.getString("nombre"));
                arrayList3.add(alumno);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return arrayList3;
    }
     

}