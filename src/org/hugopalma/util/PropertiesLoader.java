/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HUAL
 */
public class PropertiesLoader {
    private static final PropertiesLoader PROPERTIES_LOADER = 
            new PropertiesLoader();
    private Properties properties = new Properties();
    private HashMap<String, String> hashMap = new HashMap<>();

    private PropertiesLoader() {
    }

    public static PropertiesLoader getPROPERTIES_LOADER() {
        return PROPERTIES_LOADER;
    }
    
    public HashMap<String, String> load(String propertyFile) {
        hashMap.clear();
        properties.clear();
        try {// si estan en la carpeta el archivo Cual archivo? el connection.properties aqui esta :v alli tiene que estar? em simon xD
            
            properties.load(getClass().getClassLoader().getResourceAsStream(propertyFile));
        } catch (IOException ex) {
            Logger.getLogger(PropertiesLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (String key : properties.stringPropertyNames()) {
            hashMap.put(key, properties.getProperty(key));
        }
        return hashMap;
    }
    
}
