/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.hugopalma.bean.Actividad;
import org.hugopalma.bean.Materia;
import org.hugopalma.controller.ControllerActividad;
import org.hugopalma.controller.ControllerMateria;
import org.hugopalma.controller.ControllerUsuario;

/**
 *
 * @author HUAL
 */
public class CRUDActividad extends CRUDPadre implements Interface{
    private static final CRUDActividad CRUD_ACTIVIDAD = 
            new CRUDActividad();
    
    private CRUDActividad() {
    
    }
    
    public static CRUDActividad getCRUD_ACTIVIDAD() {
        return CRUD_ACTIVIDAD;
    }
    
    private TableColumn<Actividad, Integer> tableColumnId;
    private TableColumn<Actividad, String>  tableColumnDescripcion;
    private TableColumn<Actividad, Integer> tableColumnValor;
    private TableColumn<Actividad, Short> tableColumnBimestre;
    private TableColumn<Actividad, Integer> tableColumnIdMateria;
    private TableView<Actividad> tableView;
    private ObservableList observableList;
    
    public HBox gethBox() {
        hBox = new HBox();
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        
        textTitle = new Text("Actividad");
        gridPane.add(textTitle, 0, 1);
        
        hBoxBuscar = new HBox();
        
        
        textField = new TextField();
        textField.setPromptText("Buscar Actividad");
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!textField.getText().trim().isEmpty()) {
                    String text = textField.getText().trim();
                    buscar(text);
                } else {
                    updateTableViewItems();
                    
                }
                
            }
        }); 
        
        hBoxBuscar.getChildren().addAll(textField, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        agregar = new Button("Agregar");
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, CreateActividad.getCREATE_ACTIVIDAD().getGridPane());
            }
        });
        
        modificar = new Button("Modificar");
        modificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem()!= null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, UpdateActividad.getUPDATE_ACTIVIDAD().getGridPane(tableView.getSelectionModel().getSelectedItem()));
                    updateObservableList();
                }
            }
        }); 
        
        
        eliminar = new Button("Eliminar");
        eliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    eliminar(tableView.getSelectionModel().getSelectedItem().getIdActividad());
                    updateObservableList();
                    tableView.setItems(observableList);
                } 
            }
        }); 
        
         hBoxButtons.getChildren().addAll(agregar, modificar, eliminar);
         
         gridPane.add(hBoxButtons, 0, 2);
         
         tableColumnId = new TableColumn<>();
         tableColumnId.setText("ID");
         tableColumnId.setCellValueFactory(new PropertyValueFactory<>("idActividad"));
         
         tableColumnDescripcion = new TableColumn<>();
         tableColumnDescripcion.setText("Descripcion");
         tableColumnDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
         
         tableColumnValor = new TableColumn<>();
         tableColumnValor.setText("Valor");
         tableColumnValor.setCellValueFactory(new PropertyValueFactory<>("valor"));
         
         tableColumnBimestre = new TableColumn<>();
         tableColumnBimestre.setText("Bimestre");
         tableColumnBimestre.setCellValueFactory(new PropertyValueFactory<>("bimestre"));
         
         tableColumnIdMateria = new TableColumn<>("Materia");
         tableColumnIdMateria.setCellValueFactory(new PropertyValueFactory<>("Materia"));
         
         updateObservableList();
         tableView = new TableView<>(observableList);
         tableView.getColumns().addAll(tableColumnId, tableColumnDescripcion, tableColumnValor, tableColumnBimestre, tableColumnIdMateria);
         
         gridPane.add(tableView, 0, 3, 2, 1);
         hBox.getChildren().add(gridPane);
         
         return hBox;
        
    
    }
    
    public void eliminar(int id) {
        ControllerActividad.getCONTROLLER_ACTIVIDAD().delete(id);
        updateTableViewItems();
    }
    
    public void buscar(String texto) {
        observableList = FXCollections.observableArrayList(ControllerActividad.getCONTROLLER_ACTIVIDAD().buscar(texto));
        tableView.setItems(observableList);
        
    }
    public void updateObservableList() {
       observableList = FXCollections.observableArrayList(ControllerActividad.getCONTROLLER_ACTIVIDAD().getArraylist());
    }
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
}

class CreateActividad {
    private static final CreateActividad CREATE_ACTIVIDAD = 
            new CreateActividad();
    
    private CreateActividad() {
    }
    public static CreateActividad getCREATE_ACTIVIDAD() {
        return CREATE_ACTIVIDAD;
    }
    
    private GridPane gridPane;
    private Text textTitle;
    private Label descripcion;
    private Label valor;
    private TextField textField;
    private TextField textField1;
    private Button button;
    private Label label;
    private TextField textField2;
    private ComboBox<Materia> comboBox;
    private ObservableList<Materia> observableList;
    
    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25)); 
        
        textTitle = new Text("Modificar");
        gridPane.add(textTitle, 0, 0);
        
        descripcion = new Label("Descripcion");
        gridPane.add(descripcion, 0, 1);
        
        textField = new TextField();
        gridPane.add(textField, 1, 1);
        
        valor = new Label("Valor");
        gridPane.add(valor, 0, 2);
        
        textField1 = new TextField();
        gridPane.add(textField1, 1, 2);
        
        label = new Label("Bimestre");
        gridPane.add(label, 0, 3);
        
        textField2 = new TextField();
        gridPane.add(textField2, 1, 3);
        
        UpdateObservableList();
        comboBox = new ComboBox<>(observableList);
        gridPane.add(comboBox, 1, 4);
        
        button = new Button("Agregar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int valor = Integer.parseInt(textField1.getText());
                short bimestre = Short.parseShort(textField2.getText());
                agregar(textField.getText(), valor, bimestre,comboBox.getSelectionModel().getSelectedItem().getIdMateria());
            }
        });
         
        gridPane.add(button, 1, 5);
        
        
        return gridPane;
    }
    
    public void agregar(String descripcion, int valor, short bimestre, int idMateria) {
        ControllerActividad.getCONTROLLER_ACTIVIDAD().add(descripcion, valor, bimestre, idMateria);
        CRUDActividad.getCRUD_ACTIVIDAD().updateTableViewItems();
    }
    
    public void UpdateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_MATERIA().getArrayList());
    }
    
    
    


}

class UpdateActividad {
   private static final UpdateActividad UPDATE_ACTIVIDAD = 
           new UpdateActividad();
   
   private UpdateActividad() {
   }
   
   public static UpdateActividad getUPDATE_ACTIVIDAD() {
       return UPDATE_ACTIVIDAD;
   }
   
   private GridPane gridPane;
   private Text textTitle;
   private Label descripcion;
   private Label valor;
   private TextField textField;
   private TextField textField1;
   private Button button;
   private Label label;
   private TextField textField2;
   private ObservableList<Materia> observable;
   private ComboBox<Materia> comboBox;
   
   public GridPane getGridPane(Actividad actividad) {
       gridPane = new GridPane();
       gridPane.setHgap(10);
       gridPane.setVgap(10);
       gridPane.setPadding(new Insets(25, 25, 25, 25));
       
       textTitle = new Text("Modificar");
       gridPane.add(textTitle, 0, 0);
       
       descripcion = new Label("Descripcion");
       gridPane.add(descripcion, 0, 1);
       
       textField = new TextField();
       textField.setText(actividad.getDescripcion());
       gridPane.add(textField, 1, 1);
       
       valor = new Label("Valor");
       gridPane.add(valor, 0, 2);
       
       textField1 = new TextField();
       textField1.setText(String.valueOf(actividad.getValor()));
       gridPane.add(textField1, 1, 2);
       
       
       label = new Label("Bimestre");
       gridPane.add(label, 0, 3);
       
       textField2 = new TextField();
       textField2.setText(String.valueOf(actividad.getBimestre()));
       gridPane.add(textField2, 1, 3);
       
       ObservableList();
       comboBox = new ComboBox<>(observable);
       gridPane.add(comboBox, 0, 4);
       button = new Button("Modificar");
       button.setOnAction(new EventHandler() {
           @Override
           public void handle(Event event) {
              int valor = Integer.parseInt(textField1.getText());
              short bimestre = Short.valueOf(textField2.getText());
                modificar(textField.getText(), valor, bimestre, comboBox.getSelectionModel().getSelectedItem().getIdMateria(), actividad.getIdActividad());
           }
       }); 
       gridPane.add(button, 1, 5);
       
       
       return gridPane; 
   }
   
   public void modificar(String descripcion, int valor, short bimestre, int idMateria, int id) {
       ControllerActividad.getCONTROLLER_ACTIVIDAD().modify(descripcion, valor, bimestre, idMateria, id);
       CRUDActividad.getCRUD_ACTIVIDAD().updateTableViewItems();
   }
   public void ObservableList() {
       observable = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_MATERIA().getArrayList());
   }
}