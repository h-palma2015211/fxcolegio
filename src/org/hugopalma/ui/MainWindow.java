/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;


import javafx.scene.image.Image;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import org.hugopalma.controller.ControllerUsuario;

/**
 *
 * @author HUAL
 */
public class MainWindow extends Application {
   
   private VBox vBox2;
   private VBox vBox;
   private TextField textField;
   private TextField textField2;
   private Button button;
   private MenuBar menuBar2;
   private Menu menu1;
   private GridPane gridPane;
   private MenuItem menuItem;
   private MenuItem menuItem2;
   private Stage stage;
   private Scene scena2;
   private PasswordField passwordField;
   private Label label1;
   private Label label2;
   private Image imagen1;
   private ImageView ver; 
    @Override
    public void start(Stage primaryStage) {
        imagen1 = new Image("/org/hugopalma/resource/personal.png");
        ver = new ImageView();
        ver.setImage(imagen1);
        ver.setFitHeight(100);
        ver.setPreserveRatio(true);
        ver.setSmooth(true);
             
        stage = primaryStage;
         
        
        vBox = new VBox();
        
        vBox.getChildren().addAll(getMenuBar2(), getGridPane());
        vBox.setId("vBox1");
        
        
        
        
        
        
        
        scena2 = new Scene(vBox, 400, 300);
        
       scena2.getStylesheets().add(getClass().getResource("/org/hugopalma/resource/root.css").toExternalForm());
        
        stage.getIcons();
        
        stage.setTitle("LOGIN");
        stage.setScene(scena2);
        stage.getIcons().add(new Image("/org/hugopalma/resource/candados-de-oro-del-código-d-39776484.jpg")); 
        stage.show();
        
        
       
   
    
    }
    
    public VBox getGridPane() {
        vBox2 = new VBox();
        label1 = new Label("Usuario");
        label1.setMaxSize(200, 300);
        
        textField = new TextField();
        textField.setMaxSize(200, 300);
        
        label2 = new Label("Password");
        label2.setMaxSize(200, 300);
       
        passwordField = new PasswordField();
        passwordField.setMaxSize(200, 300);
        
        button = new Button("Ingresar");
        button.setDefaultButton(true);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               if (textField.getText() != null && passwordField.getText() != null ) {
                   String texto = textField.getText();
                   String password = passwordField.getText();
                   
                   String usuario = ControllerUsuario.getCONTROLLER_USUARIO().verificador(texto, password);
                  
                   if(usuario != null){
                       stage.close();
                       MenuPrincipal.getMENU_PRINCIPAL().MenuPrin();
                   } else {
                    JOptionPane.showMessageDialog(null," El usuario no existe intente de nuevo. ");
                   }
               }
               
               
            }
        });
        vBox2.getChildren().addAll( ver, label1, textField, label2, passwordField, button);
        vBox2.setAlignment(Pos.CENTER);
        
        return vBox2;
    
    }
    
     public MenuBar getMenuBar2() {
        menuBar2 = new MenuBar();
        menu1 = new Menu("_Opciones");
        menuBar2.setId("MenuBar2");
        
        menuItem = new MenuItem("_Salir");
        
        menuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               stage.close();
            }
        });
        menu1.getItems().add(menuItem);
        
        
        menuBar2.getMenus().add(menu1);
        return menuBar2;
    
     }
     
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
    