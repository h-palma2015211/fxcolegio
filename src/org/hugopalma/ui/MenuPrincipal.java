/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author HUAL
 */
public class MenuPrincipal {
   private VBox vBox;
   private VBox vBox2;
   private MenuBar menuBar;
   private Menu menu1;
   private MenuItem menuItem;
   private Stage stage;
   private TabPane tabPane;
   private Scene scene;
   private Tab dashboard;
   private Tab tabProfesor;
   private Tab tabJornada;
   private Tab tabGrado;
   private Tab tabUsuario;
   private Tab tabMateria;
   private Tab tabAlumno;
   private Tab tabAdministracion;
   private Tab tabActividad;
   private Tab tabSeccion;
   private Image image;
   private ImageView ver;
   private static final MenuPrincipal MENU_PRINCIPAL = 
           new MenuPrincipal();
   
       private MenuPrincipal() {
   
  }
   
   public static MenuPrincipal getMENU_PRINCIPAL() {
       return MENU_PRINCIPAL;
   }
   
   public void MenuPrin() {
        vBox = new VBox();
        vBox.getChildren().addAll(getMenuBar(), getTabPane());
        vBox.setStyle("-fx-background-color: #FF5722;");
        scene = new Scene(vBox, 1024, 700);
        scene.getStylesheets().add(getClass().getResource("/org/hugopalma/resource/root2.css").toExternalForm());
        
        stage = new Stage();
        stage.getIcons().add(new Image("/org/hugopalma/resource/1.jpg")); 
        stage.setTitle("FXColegio ");
        
        stage.setScene(scene);
        stage.show();
   }
    
   
   public MenuBar getMenuBar() {
        menuBar = new MenuBar();
        menu1 = new Menu("_Opcion");
        menuBar.setId("menuBar");
        
        menuItem = new MenuItem("_Salir");
        menuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               stage.close();
            }
        });
        menu1.getItems().add(menuItem);
        
        
        menuBar.getMenus().add(menu1);
        return menuBar;
    
    }   
    public TabPane getTabPane() {
        tabPane = new TabPane();
        
        dashboard = new Tab("Dashboard");
        dashboard.setId("dashboard");
        dashboard.setContent(Dashboard.getDASHBOARD().getHbox());
        
        tabProfesor = new Tab("Profesores");
        tabProfesor.setId("tabProfesor");
        tabProfesor.setContent(CRUDProfesor.getCRUD_PROFESOR().getHbox());
        
        tabMateria = new Tab("Materias");
        tabMateria.setId("tabMateria");
        tabMateria.setContent(CRUDMateria.getCRUD_MATERIA().gethBox());
        
        tabJornada = new Tab("Jornadas");
        tabJornada.setId("tabJornada");
        tabJornada.setContent(CRUDJornada.getCRUD_JORNADA().gethBox());
        
        tabUsuario = new Tab("Usuarios");
        tabUsuario.setId("tabUsuario");
        tabUsuario.setContent(CRUDUsuario.getCRUD_USUARIO().gethBox());
        
        tabAdministracion = new Tab("Administracion");
        tabAdministracion.setId("tabAdministracion");
        tabAdministracion.setContent(CRUDAdministracion.getCRUD_ADMINISTRACION().gethBox());
        
        tabActividad = new Tab("Actividades");
        tabActividad.setId("tabActividad");
        tabActividad.setContent(CRUDActividad.getCRUD_ACTIVIDAD().gethBox());
        
        tabGrado = new Tab("Grados");
        tabGrado.setId("tabGrado");
        tabGrado.setContent(CRUDGrado.getCRUD_GRADO().gethBox());
        
        tabAlumno = new Tab("Alumnos");
        tabAlumno.setId("tabAlumno");
        tabAlumno.setContent(CRUDAlumno.getCRUD_ALUMNO().gethBox());
        
        tabSeccion = new Tab("Secciones");
        tabSeccion.setId("tabSeccion");
        tabSeccion.setContent(CRUDSeccion.getCRUD_SECCION().gethBox());
        
        tabPane.setId("tabPane");
        tabPane.getTabs().addAll(dashboard,tabUsuario, tabProfesor, tabJornada,
                tabMateria, tabAdministracion, tabActividad, tabGrado,
                tabAlumno, tabSeccion);
        return tabPane;
    }
}
