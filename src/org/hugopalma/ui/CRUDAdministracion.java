/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.hugopalma.bean.Administracion;
import org.hugopalma.controller.ControllerAdministracion;

/**
 *
 * @author HUAL
 */
public class CRUDAdministracion extends CRUDPadre {
    private static final CRUDAdministracion CRUD_ADMINISTRACION =
            new CRUDAdministracion();
    
    private TableColumn<Administracion, Integer> tableColumnId;
    private TableColumn<Administracion, String> tableColumnNombre;
    private TableColumn<Administracion, String> tableColumnPuesto;
    private TableColumn<Administracion, Integer> tableColumnsueldo;
    private TableView<Administracion> tableView;
    private ObservableList observableList;
    
    private CRUDAdministracion() {
        
    }
    public static CRUDAdministracion getCRUD_ADMINISTRACION() {
        return CRUD_ADMINISTRACION;
    }
    
    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        textTitle = new Text();
        
        hBoxBuscar = new HBox(10);
        
        textField = new TextField();
        textField.setPromptText("Buscar un empleado");
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                if (!textField.getText().trim().isEmpty()) {
                    String text = textField.getText().trim();
                    buscar(text);
                }else{
                    updateTableViewItems();
                }
                
            }
        });
        
        
        hBoxBuscar.getChildren().addAll(textField, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox();
        agregar = new Button("Nuevo");
        agregar.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, CreateAdministracion.getCREATE_ADMINISTRACION().getGridPane());
            }
        });
        
       
        modificar = new Button("Modificar");
        modificar.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                if (tableView.getSelectionModel().getSelectedItems() != null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, UpdateAdministracion.getUPDATE_ADMINISTRACION().getGridPane(tableView.getSelectionModel().getSelectedItem()));
                    updateObservableList();
                }
            }
        }); 
        
        eliminar = new Button("Eliminar");
        eliminar.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                   eliminar(tableView.getSelectionModel().getSelectedItem().getId()); 
                   updateObservableList();
                   tableView.setItems(observableList);
                }
            }
        });
        
        hBoxButtons.getChildren().addAll(agregar, modificar, eliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnId = new TableColumn<>();
        tableColumnId.setText("ID");
        tableColumnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnId.setMinWidth(100);
        
        
        tableColumnNombre = new TableColumn<>();
        tableColumnNombre.setText("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tableColumnNombre.setMinWidth(200);
        
        tableColumnPuesto = new TableColumn<>();
        tableColumnPuesto.setText("Puesto");
        tableColumnPuesto.setCellValueFactory(new PropertyValueFactory<>("puesto"));
        
        tableColumnsueldo = new TableColumn<>();
        tableColumnsueldo.setText("Sueldo");
        tableColumnsueldo.setCellValueFactory(new PropertyValueFactory<>("sueldo"));
        
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnId, tableColumnNombre, tableColumnPuesto,tableColumnsueldo);
        gridPane.add(tableView, 0,3, 2, 1);
        
        hBox.getChildren().add(gridPane);
        return hBox;
       
    }
    public void eliminar(int id) {
        ControllerAdministracion.getCONTROLLER_ADMINISTRACION().delete(id);
        updateTableViewItems();
    }
    public void buscar(String texto) {
        observableList = FXCollections.observableArrayList(ControllerAdministracion.getCONTROLLER_ADMINISTRACION().buscar(texto));
        tableView.setItems(observableList);
    }
    public void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerAdministracion.getCONTROLLER_ADMINISTRACION().getArrayList());
    }
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    
    
    
    
    
    
}


class CreateAdministracion {
    public static CreateAdministracion CREATE_ADMINISTRACION = new
        CreateAdministracion();
    
    private CreateAdministracion() {
    }
    public static CreateAdministracion getCREATE_ADMINISTRACION() {
        return CREATE_ADMINISTRACION;
    }
    
    
    private GridPane gridPane1;
    private Text textTitulo;
    private Label label;
    private Label label1;
    private Label label2;
    private TextField textField;
    private TextField textField1;
    private TextField textField2;
    private Button button;
            
    
    
    public GridPane getGridPane() {
        gridPane1 = new GridPane();
        
        gridPane1.setHgap(10);
        gridPane1.setVgap(10);
        gridPane1.setPadding(new Insets(25, 25, 25, 25));
        
        textTitulo = new Text("Agregar");
        gridPane1.add(textTitulo, 0, 0);
        
        
        label = new Label("Nombre");
        gridPane1.add(label, 0, 1);
        
        textField = new TextField();
        gridPane1.add(textField, 1, 1);
        
        label1 = new Label("Puesto");
        gridPane1.add(label1, 0, 2);
        
        textField1 = new TextField();
        gridPane1.add(textField1, 1, 2);
        
        label2 = new Label("sueldo");
        gridPane1.add(label2, 0, 3);
        
        textField2 = new TextField();
        gridPane1.add(textField2, 1, 3);
        
        button = new Button("Agregar");
        button.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                int sueldo = Integer.parseInt(textField2.getText());
                agregar(textField.getText(), textField1.getText(), sueldo );
            }
        }); 
        gridPane1. add(button, 1, 4);
       
        
        return gridPane1;
        
    }
    public void agregar(String nombre, String puesto, int sueldo) {
        ControllerAdministracion.getCONTROLLER_ADMINISTRACION().add(nombre, puesto, sueldo);
        CRUDAdministracion.getCRUD_ADMINISTRACION().updateTableViewItems();
    }
}


class UpdateAdministracion {
    private static final UpdateAdministracion UPDATE_ADMINISTRACION = new
        UpdateAdministracion();
    
    private UpdateAdministracion() {
    
    }
    
    
    public static UpdateAdministracion getUPDATE_ADMINISTRACION() {
        return UPDATE_ADMINISTRACION;
    }
    
     private GridPane gridPane;
    private Text textTitle;
    private Label label;
    private TextField textField;
    private Label label1;
    private TextField textField1;
    private Label label2;
    private TextField textField2;
    private Button button;
    
    public GridPane getGridPane(Administracion administracion) {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25)); 
        
        textTitle = new Text("Modificar");
        gridPane.add(textTitle, 0, 0);
        
        label = new Label("nombre");
        gridPane.add(label, 0, 1);
        
        textField = new TextField();
        textField.setText(administracion.getNombre());
        gridPane.add(textField, 1, 1);
        
        label1 = new Label("Puesto");
        gridPane.add(label1, 0, 2);
        
        textField1 = new TextField();
        textField1.setText(administracion.getPuesto());
        gridPane.add(textField1, 1, 2);
        
        label2 = new Label("sueldo");
        gridPane.add(label2, 0, 3);
        
        textField2 = new TextField();
        textField2.setText(String.valueOf(administracion.getSueldo()));
        gridPane.add(textField2, 1, 3);
        
        button = new Button("Modificar");
        button.setOnAction(new EventHandler() {
            @Override
            public void handle(Event event) {
                int sueldo = Integer.parseInt(textField2.getText());
                modificar(textField.getText(), textField1.getText(), sueldo, administracion.getId());
                
            }
        });
        gridPane.add(button, 1, 4);
        
        return gridPane;
        
    }
    public void modificar(String nombre, String puesto, int sueldo, int id) {
    ControllerAdministracion.getCONTROLLER_ADMINISTRACION().modify(nombre, puesto, sueldo, id);
    CRUDAdministracion.getCRUD_ADMINISTRACION().updateTableViewItems();
    
    }
    
    
    
    

}
