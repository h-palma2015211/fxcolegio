/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import groovy.lang.PropertyValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.hugopalma.bean.Alumno;
import org.hugopalma.bean.Materia;
import org.hugopalma.bean.NotasBimestrales;
import org.hugopalma.controller.ControllerNotasBimestrales;

/**
 *
 * @author HUAL
 */
public class Nota extends Stage{
    private static final Nota NOTA = new Nota();
    private VBox vBox;
    private Scene scene;
    private Label label;
    private Label label3;
    private Label label1;
    private Label label2;
    
    private Stage stage;
    private Nota() {
    
    }
    
    public static Nota getNOTA() {
        return NOTA;
    } 
    
 
  

    
    public void stageNota(int idAlumno) {
        vBox = new VBox(10);
        
       
   
        NotasBimestrales nota = ControllerNotasBimestrales.
                getCONTROLLER_NOTAS_BIMESTRALES().search(idAlumno);
        
        
        label = new Label("Alumno");
        label3 = new Label("NOTA OBTENIDA");
        label1 = new Label(nota.getAlumno().getNombre());
        label2 = new Label(String.valueOf(nota.getNota()));
        
        
        vBox.getChildren().addAll(label, label1, label3, label2);
       
        stage = new Stage();
        
       
        scene = new Scene(vBox, 200, 200);
        stage.setScene(scene);
        stage.show();
        
    }
   
    
    
}
