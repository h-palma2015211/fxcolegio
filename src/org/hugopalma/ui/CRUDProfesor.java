/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.hugopalma.bean.Materia;
import org.hugopalma.bean.Profesor;
import org.hugopalma.bean.Seccion;
import org.hugopalma.conecction.SQLDatabaseConnection;
import org.hugopalma.controller.ControllerMateria;
import org.hugopalma.controller.ControllerProfesor;
import org.hugopalma.controller.ControllerProfesorSeccion;
import org.hugopalma.controller.ControllerSeccion;

/**
 *
 * @author HUAL
 */
public class CRUDProfesor extends CRUDPadre implements Interface {
    private static final CRUDProfesor CRUD_PROFESOR = new CRUDProfesor();
    
    private CRUDProfesor() {
    }
    
    public static CRUDProfesor getCRUD_PROFESOR() {
        return CRUD_PROFESOR;
    }
    
    private TableColumn<Profesor, Integer> tableColumnId;
    private TableColumn<Profesor, String>  tableColumnNombre;
    private TableColumn<Profesor, String> tableColumnDireccion;
    private TableColumn<Profesor, String> tableColumnDpi;
    private TableColumn<Profesor, Materia> tableColumnMateria;
    private TableView<Profesor> tableView;
    private ObservableList observableList;
    private Button boton;
    
    public HBox getHbox() {
     hBox = new HBox();
     
     gridPane = new GridPane();
     gridPane.setHgap(10);
     gridPane.setVgap(10);
     gridPane.setPadding(new Insets(25, 25, 25, 25));
     
     textTitle = new Text("Profesores");
     gridPane.add(textTitle, 0, 0);
     
     hBoxBuscar = new HBox(10);
     textField = new TextField();
    
     buttonBuscar = new Button("Buscar");
     buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent event) {
             if (!textField.getText().trim().isEmpty()) {
                 String text = textField.getText().trim();
                 buscar(text);
             } else {
                 updateTableViewItems();
             }
         }  
     }); 
     hBoxBuscar.getChildren().addAll(textField, buttonBuscar);

     gridPane.add(hBoxBuscar, 0, 1);
     
     hBoxButtons = new HBox(10);
     
     agregar = new Button("Agregar");
     agregar.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent event) {
            hBox.getChildren().clear();
            hBox.getChildren().addAll(gridPane, CreateProfesor.getCREATE_PROFESOR().getGridPane());
         }
     });
     
     modificar  = new Button("Modificar");
     modificar.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent event) {
             if (tableView.getSelectionModel().getSelectedItem() != null) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, UpdateProfesor.getUPDATE_PROFESOR().getGridPane(tableView.getSelectionModel().getSelectedItem()));
                updateObservableList();
            }
        }
     });
     
     eliminar = new Button("Eliminar");
     eliminar.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent event) {
             if (tableView.getSelectionModel().getSelectedItem() != null) {
                 eliminar(tableView.getSelectionModel().getSelectedItem().getIdProfesor());
                 updateObservableList();
                 tableView.setItems(observableList);
             }
         }
     }); 
    boton = new Button("Secciones");
    boton.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent event) {
             if (tableView.getSelectionModel().getSelectedItem() != null) {
                 hBox.getChildren().clear();
                 hBox.getChildren().addAll(gridPane, Secciones.getSECCIONES().getGridPane(tableView.getSelectionModel().getSelectedItem().getIdProfesor()));
             }
         }
     }); 
    hBoxButtons.getChildren().addAll(agregar, modificar, eliminar, boton);
    
    gridPane.add(hBoxButtons, 0, 2);
    
    tableColumnId = new TableColumn<>("ID");
    tableColumnId.setCellValueFactory(new PropertyValueFactory<>("idProfesor")); 
    
    tableColumnNombre = new TableColumn<>("Nombre");
    tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
    
    tableColumnDireccion = new TableColumn<>("Direccion");
    tableColumnDireccion.setCellValueFactory(new PropertyValueFactory<>("direccion"));
    
    tableColumnDpi = new TableColumn<>("DPI");
    tableColumnDpi.setCellValueFactory(new PropertyValueFactory<>("dpi"));
    
    tableColumnMateria = new TableColumn<>("Materias");
    tableColumnMateria.setCellValueFactory(new PropertyValueFactory<>("Materia"));
    
    
    updateObservableList();
    tableView = new TableView<>(observableList);
    tableView.getColumns().addAll(tableColumnId, tableColumnNombre, tableColumnDireccion,
           tableColumnDpi, tableColumnMateria);
    
    
    gridPane.add(tableView, 0, 3, 2, 1);
    
    hBox.getChildren().addAll(gridPane);
    
    
    return hBox;
    
    
    
    }
    
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    public void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerProfesor.getCONTROLLER_PROFESOR().getArrayList());
    }

    private void buscar(String texto) {
        observableList = FXCollections.observableArrayList(ControllerProfesor.getCONTROLLER_PROFESOR().buscar(texto));
        tableView.setItems(observableList);
    }
    private void eliminar(int idProfesor) {
        ControllerProfesor.getCONTROLLER_PROFESOR().delete(idProfesor);
        updateObservableList();
    }
    
    
}


class CreateProfesor{
    private static final CreateProfesor CREATE_PROFESOR = new CreateProfesor();
    
    private CreateProfesor() {
    }
    
    public static CreateProfesor getCREATE_PROFESOR(){
        return CREATE_PROFESOR;
    }
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textFieldN;
    private Label labelDireccion;
    private TextField textFieldDireccion;
    private Label labelDpi;
    private TextField textFieldDPI;
    private Label labelMateria;
    private Button button;
    private ComboBox<Materia> comboBoxMateria;
    private ObservableList<Materia> observableListMateria;
    
    
    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Agregar");
        gridPane.add(textTitle, 0, 0);
        
        labelMateria = new Label("Materia");
        gridPane.add(labelMateria, 0, 1);
        UpdateObservableListMateria();
        comboBoxMateria = new ComboBox<>(observableListMateria);
        gridPane.add(comboBoxMateria, 1, 1);
        
        labelNombre = new Label("Nombre");
        gridPane.add(labelNombre, 0, 2);
        
        textFieldN = new TextField();
        gridPane.add(textFieldN, 1, 2);
        
        labelDireccion = new Label("Direccion");
        gridPane.add(labelDireccion, 0, 3);
        
        textFieldDireccion = new TextField();
        gridPane.add(textFieldDireccion, 1, 3);
        
        labelDpi = new Label("DPI");
        gridPane.add(labelDpi, 0, 4);
        
        textFieldDPI = new TextField();
        gridPane.add(textFieldDPI, 1, 4);
        
        button = new Button("Agregar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                agregar(comboBoxMateria.getSelectionModel().getSelectedItem(),
                        textFieldN.getText(),
                        textFieldDireccion.getText(),
                        textFieldDPI.getText());
            }
        }); 
        gridPane.add(button, 1, 5);
        return gridPane;
    }

public void UpdateObservableListMateria() {
    observableListMateria = FXCollections.observableArrayList
        (ControllerMateria.getCONTROLLER_MATERIA().getArrayList());
}    
private void agregar(Materia materia, String nombre, String direccion, String dpi) {
    ControllerProfesor.getCONTROLLER_PROFESOR().add(nombre, direccion, dpi, materia.getIdMateria());
    CRUDProfesor.getCRUD_PROFESOR().updateTableViewItems();
}


}

class UpdateProfesor {
    private static final UpdateProfesor UPDATE_PROFESOR = new UpdateProfesor();
    
    private UpdateProfesor() {}

    public static UpdateProfesor getUPDATE_PROFESOR() {
       return UPDATE_PROFESOR;
    }
    
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textFieldN;
    private Label labelDireccion;
    private TextField textFieldDireccion;
    private Label labelDpi;
    private TextField textFieldDPI;
    private Label labelMateria;
    private Button button;
    private ComboBox<Materia> comboBoxMateria;
    private ObservableList<Materia> observableListMateria;
    
    public GridPane getGridPane(Profesor profesor) {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Agregar");
        gridPane.add(textTitle, 0, 0);
        
        labelMateria = new Label("Materia");
        gridPane.add(labelMateria, 0, 1);
        UpdateObservableListMateria();
        comboBoxMateria = new ComboBox<>(observableListMateria);
        gridPane.add(comboBoxMateria, 1, 1);
        
        labelNombre = new Label("Nombre");
        gridPane.add(labelNombre, 0, 2);
        
        textFieldN = new TextField();
        textFieldN.setText(profesor.getNombre());
        gridPane.add(textFieldN, 1, 2);
        
        labelDireccion = new Label("Direccion");
        gridPane.add(labelDireccion, 0, 3);
        
        textFieldDireccion = new TextField();
        textFieldDireccion.setText(profesor.getDireccion());
        gridPane.add(textFieldDireccion, 1, 3);
        
        labelDpi = new Label("DPI");
        gridPane.add(labelDpi, 0, 4);
        
        textFieldDPI = new TextField();
        textFieldDPI.setText(profesor.getDpi());
        gridPane.add(textFieldDPI, 1, 4);
        
        button = new Button("Modificar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               modificar(comboBoxMateria.getSelectionModel().getSelectedItem(), 
                       textFieldN.getText(), 
                       textFieldDireccion.getText(), 
                       textFieldDPI.getText(),
                       profesor.getIdProfesor());
            }
        }); 
        gridPane.add(button, 1, 5);
        return gridPane;
    }
    
    public void UpdateObservableListMateria() {
    observableListMateria = FXCollections.observableArrayList
        (ControllerMateria.getCONTROLLER_MATERIA().getArrayList());
    }  
    
    public void modificar (Materia materia, String nombre, String direccion, String dpi, int idProfesor) {
        ControllerProfesor.getCONTROLLER_PROFESOR().modify(nombre, direccion, dpi, materia.getIdMateria(), idProfesor);
        CRUDProfesor.getCRUD_PROFESOR().updateTableViewItems();
    }
    
}

class Secciones{
    private static final Secciones SECCCIONES = new Secciones();
    private Secciones() {
    }
    public static Secciones getSECCIONES() {
        return SECCCIONES;
    }
    private GridPane gridPane;
    private ObservableList<Seccion> observableListSeccion1;
    private ObservableList<Seccion> observableListSeccion2;
    private HBox hBox;
    private ListView<Seccion> listView1;
    private ListView<Seccion> listView2;
    private Button button;
    private Button button1;
    private VBox vBox;
    private HBox hBox2; 
    
    
    public GridPane getGridPane(int idProfesor) {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        
        hBox = new HBox(10);
        UpdateObservableListSeccion1(idProfesor);
        listView1 = new ListView<>(observableListSeccion1);
        hBox.getChildren().add(listView1);
        gridPane.add(hBox, 0, 1);
        vBox = new VBox(10);
        button = new Button("Agregar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (listView1.getSelectionModel().getSelectedItem() != null) {
                    ControllerProfesorSeccion.getCONTROLLER_PROFESOR_SECCION().add(listView1.getSelectionModel().getSelectedItem().getIdSeccion(), idProfesor);
                    UpdateObservableListSeccion1(idProfesor);
                    UpdateObservableListSeccion2(idProfesor);
                    listView1.setItems(observableListSeccion1);
                    listView2.setItems(observableListSeccion2);
                }
            }
        }); 
        button1 = new Button("Eliminar");
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (listView2.getSelectionModel().getSelectedItem() != null) {
                    ControllerProfesorSeccion.getCONTROLLER_PROFESOR_SECCION().delete(listView2.getSelectionModel().getSelectedItem().getIdSeccion());
                    UpdateObservableListSeccion1(idProfesor);
                    UpdateObservableListSeccion2(idProfesor);
                    listView1.setItems(observableListSeccion1);
                    listView2.setItems(observableListSeccion2);
                    System.out.println(listView2.getSelectionModel().getSelectedItem().getIdSeccion());
                }
            }
        }); 
        vBox.getChildren().addAll(button, button1);
        gridPane.add(vBox, 2, 1);
        
        hBox2 = new HBox(10);
        listView2 = new ListView<>();
        UpdateObservableListSeccion2(idProfesor);
        listView2.getItems().setAll(observableListSeccion2);
        hBox2.getChildren().add(listView2);
        gridPane.add(hBox2, 3, 1);
        return gridPane;
    }
    
    public void UpdateObservableListSeccion1(int idProfesor) {
        observableListSeccion1 = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_SECCION().getArrayList3(idProfesor));
    }
    public void UpdateObservableListSeccion2(int idProfesor) {
        observableListSeccion2 = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_SECCION().getArrayList2(idProfesor));
    
    }
    
    
}
