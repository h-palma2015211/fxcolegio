/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hugopalma.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.hugopalma.bean.Grado;
import org.hugopalma.controller.ControllerGrado;
import org.hugopalma.controller.ControllerUsuario;

/**
 *
 * @author HUAL
 */
public class CRUDGrado extends CRUDPadre {
    private static final CRUDGrado CRUD_GRADO = new CRUDGrado();
    
    private CRUDGrado() {
    }
    
    public static CRUDGrado getCRUD_GRADO() {
        return CRUD_GRADO;
    }
    
    private TableColumn<Grado, Integer> tableColumnId;
    private TableColumn<Grado, String> tableColumnNombre;
    private ObservableList observableList;
    private TableView<Grado> tableView;
    
        
    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        hBoxBuscar = new HBox(10);
        
        textTitle = new Text("GRADOS");
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textField = new TextField();
        textField.setPromptText("Buscar Grado");
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               if (!textField.getText().trim().isEmpty()) {
                   String texto = textField.getText().trim();
                   buscar(texto);
               } else {
                   updateTableViewItems();
               }
            }
        });
         hBoxBuscar.getChildren().addAll(textField, buttonBuscar);
         gridPane.add(hBoxBuscar, 0, 1);
         
         hBoxButtons = new HBox(10);
         
         agregar = new Button("Agregar");
         agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, CreateGrado.getCREATE_GRADO().getCridPane());
            }
        }); 
         modificar = new Button("Modificar");
         modificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(tableView.getSelectionModel().getSelectedItem() != null) {
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, UpdateGrado.getUPDATE_GRADO().getGridPane(tableView.getSelectionModel().getSelectedItem()));
                    updateObservableList();
                     
                }
            }
        });
     
         eliminar = new Button("Eliminar");
         eliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    eliminar(tableView.getSelectionModel().getSelectedItem().getIdGrado());
                    updateObservableList();
                    tableView.setItems(observableList);                    

                }
            }
        }); 
         
         hBoxButtons.getChildren().addAll(agregar, modificar, eliminar);
         gridPane.add(hBoxButtons, 0, 2);
         
         tableColumnId = new TableColumn<>();
         tableColumnId.setText("ID");
         tableColumnId.setCellValueFactory(new PropertyValueFactory<>("idGrado"));
         
         tableColumnNombre = new TableColumn<>();
         tableColumnNombre.setText("Nombre");
         tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
         
         
         updateObservableList();
         
         tableView = new TableView<>(observableList);
         tableView.getColumns().addAll(tableColumnId, tableColumnNombre);
         gridPane.add(tableView, 0, 3, 2, 1);
        
         hBox.getChildren().add(gridPane);
         
        return hBox;
    
    }
    
    
    
    public void eliminar(int id) {
        ControllerGrado.getCONTROLLER_GRADO().delete(id);
        updateObservableList();
    }
    
    public void buscar(String texto) {
        observableList = FXCollections.observableArrayList(ControllerGrado.getCONTROLLER_GRADO().buscar(texto));
        tableView.setItems(observableList);
        
    }
    public void updateObservableList() {
       observableList = FXCollections.observableArrayList(ControllerGrado.getCONTROLLER_GRADO().getArrayList());
    }
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    
    
    
}

class CreateGrado {
    private static final CreateGrado CREATE_GRADO = new  CreateGrado();
    
    private CreateGrado() {
    }
    
    public static CreateGrado getCREATE_GRADO() {
        return CREATE_GRADO;
    }
    
    private GridPane gridPane;
    private Text text;
    private Label label;
    private TextField textField;
    private Button button;
    
    
    public GridPane getCridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        text = new Text("Agregar");
        gridPane.add(text, 0, 0);
        
        label = new Label("Nombre");
        gridPane.add(label, 0, 1);
        
        textField = new TextField();
        gridPane.add(textField, 1, 1);
        
        button = new Button("Agregar");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                agregar(textField.getText());
            }
        });
        gridPane.add(button, 1, 2);
        
        return gridPane;
        
    } 
    
    public void agregar(String nombre) {
        ControllerGrado.getCONTROLLER_GRADO().add(nombre);
        CRUDGrado.getCRUD_GRADO().updateTableViewItems();
    }
}


class UpdateGrado {
    private static final UpdateGrado UPDATE_GRADO = new UpdateGrado();
    
    private UpdateGrado() {
    }
    
    public static UpdateGrado getUPDATE_GRADO() {
        return UPDATE_GRADO;
    }

    
    private GridPane gridPane;
    private Text text;
    private Label label;
    private TextField textField;
    private Button button;
    
    
   public GridPane getGridPane(Grado grado) {
       gridPane = new GridPane();
       gridPane.setHgap(10);
       gridPane.setVgap(10);
       gridPane.setPadding(new Insets(25, 25, 25, 25));
       
       text = new Text("Modificar");
       gridPane.add(text, 0, 0);

       label = new Label("Nombre");
       gridPane.add(label, 0, 1);
       
       textField = new TextField();
       textField.setText(grado.getNombre());
       gridPane.add(textField, 1, 1);
       
       button = new Button("Modificar");
       button.setOnAction(new EventHandler<ActionEvent>() {
           @Override
           public void handle(ActionEvent event) {
               modificar(textField.getText(), grado.getIdGrado());
           }
       }); 
       gridPane.add(button, 1, 2);
       
       return gridPane;
       
   }

   public void modificar(String nombre, int id) {
       ControllerGrado.getCONTROLLER_GRADO().modify(nombre, id);
       CRUDGrado.getCRUD_GRADO().updateTableViewItems();
   }
}